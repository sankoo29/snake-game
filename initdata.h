#define MAX 50

typedef struct node {
	char ch;
	struct node *prev , *next;
}node;

typedef struct san {
	node *head[MAX];
	int tab;
	int maxy;
	int maxx[MAX];
}san;

void init(san *f);
int insert(san *f, char ch, int y , int x);
void deletec(san *f, int y , int x);
void backspace(san *f, int y , int x);
void insertline(san *f ,int y , int x);
void deleteline(san *f ,int y , int x);
